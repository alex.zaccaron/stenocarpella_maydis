# *Stenocarpella maydis*
## Genes


This repository

[Zaccaron, A.Z., Woloshuk, C.P. and Bluhm, B.H., 2017. Comparative genomics of maize ear rot pathogens reveals expansion of carbohydrate-active enzymes and secondary metabolism backbone genes in Stenocarpella maydis. Fungal Biology, 121(11), pp.966-983](http://www.sciencedirect.com/science/article/pii/S1878614617301095)
